%include "lib.inc"
%include "dict.inc"
%include "words.inc"
section .bss
stdin_string: resb 256
section .rodata
len_err_mes: db "Length is so big!", 0
find_err_mes: db "This key isn`t in dictionary", 0
section .text
global _start
_start:
	mov rdi, stdin_string
	push rdi
	mov rsi, 256
	call read_string
	cmp rax, 256
	je .long_err
	pop rdi
	mov rsi, first_word
	call find_word
	test rax, rax
	je .not_find
	mov rdi, rax
	call get_word
	mov rdi, rax
	call print_string
	xor rdi, rdi
	jmp .r
	.long_err:
		mov rdi, len_err_mes
		call print_err
		jmp .r
	.not_find:
		mov rdi, find_err_mes
		call print_err
	.r:
		jmp exit
	
	
