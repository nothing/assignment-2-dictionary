%define SYSCALL_EXIT  60
%define SYSCALL_WRITE 1
%define SYSCALL_READ  0
%define STDOUT_DESC   1
%define STDIN_DESC    0
%define STDERR_DESC   2 
%define CODE_OF_ZERO  '0'
%define CODE_OF_NINE  '9'
%define SWITCHER_TO_ASCII 48
%define test_TAB 0x9
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_string
global skip_cringe
global print_err
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax
    mov rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
    	cmp byte[rdi+rax], 0
	je .end
	inc rax
	jmp .loop
    .end:
    ret    

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT_DESC
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT_DESC
    mov rsi, rsp 
    mov rdx, 1
    syscall
    pop r8
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT_DESC
    mov rsi, `\n`
    mov rdx, 1
    syscall 
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    xor rdx, rdx
    push r14
    push r13
    push r12
    mov r14, rsp
    xor r13, r13
    mov rax, rdi
    mov r12, 10
    .loop:
    xor rdx, rdx
    div r12
    add rdx, SWITCHER_TO_ASCII
    inc r13
    dec rsp
    mov byte[rsp], dl
    test rax, rax
    jne .loop
    .end:
    mov rax, SYSCALL_WRITE
    mov rdi, STDOUT_DESC
    mov rsi, rsp
    mov rdx, r13
    syscall
    mov rsp, r14
    pop r12
    pop r13
    pop r14
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor  rax, rax
    test rdi, rdi
    jns   .pos
    neg  rdi
    push rdi
    mov  rdi, '-'
    call print_char
    pop  rdi
    .pos:
    call print_uint
    .r:
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    push rdx
    push rdi
    push rsi
    call string_length
    mov r12, rax
    mov rdi, [rsp]
    call string_length
    mov r9, rax
    cmp r9, r12
    pop rsi
    pop rdi
    jne .end_ne
    xor rax, rax
    .loop_eq:
    	cmp rax, r9
	je .end_e
	mov dl, byte[rdi+rax]
        inc rax
	cmp dl, byte[rsi+rax-1]
    	je .loop_eq
	jmp .end_ne
    .end_e:
	mov rax, 1
    	jmp .r
    .end_ne:
	xor rax, rax
    .r:
 	pop rdx	
    	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push 0
    mov rax, SYSCALL_READ
    mov rdi, STDIN_DESC
    mov rsi, rsp
    mov rdx, 1
    syscall
    cmp rax, -1
    mov al, byte[rsp]
    je .err
    jmp .r
    .err:
    xor rax, rax
    .r:
    add rsp, 8
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    xor r14, r14
    .loop_1:
    call read_char
    cmp rax, ' '
    je .loop_1 
    cmp rax, `\t`
    je .loop_1
    cmp rax, `\n`
    je .loop_1
    cmp rax, -1
    je .err
    test rax, rax
    je .end
    .loop_2:
    cmp r13, r14
    jb .err
    mov [r12+r14], rax
    inc r14
    call read_char
    test rax, rax
    je .end
    cmp rax, ' '
    je .end
    cmp rax, `\t`
    je .end
    jmp .loop_2
    .err:
    xor rax, rax
    jmp .r
    .end:
    inc r14
    mov byte[r12+r14], '0'
    mov rax, r12
    dec r14
    mov rdx, r14
    .r:
    pop r14
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    push r12
    push r13
    push r14
    push r15
    mov r13, CODE_OF_ZERO
    mov r14, CODE_OF_NINE
    mov r15, 10
    cmp byte[rdi+rsi], r13b
    jb .err
    cmp r14b, byte[rdi+rsi]
    jb .err
    add al, byte[rdi]
    sub rax, SWITCHER_TO_ASCII
    inc rsi
    .loop:
    xor rdx, rdx
    cmp byte[rdi+rsi], r13b
    jb .end
    cmp r14b, byte[rdi+rsi]
    jb .end
    mul r15
    movzx rdx, byte[rdi+rsi]
    add rax, rdx
    sub rax, SWITCHER_TO_ASCII
    inc rsi 
    jmp .loop
    .err:
    xor rdx, rdx
    jmp .r
    .end:
    mov rdx, rsi
    .r:
    pop r15
    pop r14
    pop r13
    pop r12
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    push r9
    push r10
    push r11
    mov  r11, '-'
    xor rsi, rsi
    inc rsi
    cmp byte[rdi+rsi], ' '
    je .err
    cmp word[rdi+rsi], `\t`
    je .err
    cmp word[rdi+rsi], `\n`
    je .err
    dec rsi
    cmp byte[rdi], r11b
    je .neg
    call parse_uint
    jmp .r
    .neg:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    jmp .r
    .err:
    xor rdx, rdx
    .r:
    pop r11
    pop r10
    pop r9
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    push r12
    push r13
    push rdx
    mov r12, rdi
    mov r13, rsi
    call string_length
    pop rdx
    mov rdi, r12
    mov rsi, r13
    cmp rdx, rax
    jb .end_err
    xor r13, r13
    .loop:
    mov r12, [rdi]
    mov qword[rsi], r12
    inc rdi
    inc rsi
    inc r13
    cmp rax, r13
    jb .end
    jmp .loop
    .end_err:
    xor rax, rax
    .end:
    pop r13
    pop r12
    ret

; Читает строку из STDIN
read_string:
    xor rax, rax
    mov rdx, rsi       
    mov rsi, rdi        
    mov rdi, STDIN_DESC
    mov r12, rsi
    syscall
    mov rsi, r12
    test rax, rax
    je .err
    js .err
    add rsi, rax
    dec rsi
    cmp byte[rsi], `\n`
    jne .r
    mov byte[rsi], 0
    sub rsi, rax
    dec rax
    .r: 
    	ret
    .err:
        xor rax, rax
    ret

;Пропускает нули
skip_cringe:
    .loop:
    inc rdi
    cmp byte[rdi], 0
    je .loop
    mov rax, rdi
    ret

print_err:
    xor rax, rax
    mov r12, rdi
    call string_length
    mov rdx, rax
    mov rax, SYSCALL_WRITE
    mov rsi, r12
    mov rdi, STDERR_DESC
    syscall
    mov rdi, r12
    ret
