%include "lib.inc"
%define QWORD_SIZE 8
global find_word
global get_word
section .text
	find_word:
	;rdi - указатель на строку
	;rsi - указатель на начало списка
	push rbp
	push rdi
	xor rbp, rbp
	xor rax, rax
	.loop:
		xor r11, r11
		mov r11, rsi
		mov ebp, dword[rsi]
		lea rsi, [rsi+QWORD_SIZE]
		push rsi
		call string_equals
		pop rsi
		test rax, rax
		jne .find
		test rbp, rbp
		je .not_find
		mov esi, ebp
		jmp .loop
	.not_find:
		xor rax, rax
		jmp .r
	.find:
		mov rax, r11
	.r:
		pop rdi
		pop rbp
	ret

	get_word:
    		lea rdi, [rdi + QWORD_SIZE]
		push rdi
    		call string_length
		pop rdi
    		lea rdi, [rdi + rax]
		call skip_cringe
   	ret
		
		
